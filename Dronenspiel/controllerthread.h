#ifndef CONTROLLERTHREAD_H
#define CONTROLLERTHREAD_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>



class ControllerThread : public QThread
{
public:
    void run();
    ControllerThread();
    ~ControllerThread();
};

#endif // CONTROLLERTHREAD_H
