#-------------------------------------------------
#
# Project created by QtCreator 2013-09-24T14:38:58
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += sql

QTPLUGIN += qsqlite


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Dronenspiel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    kommunikation.cpp \
    kommandos.cpp \
    gamepad.cpp \
    spielerdialog.cpp \
    datenbankmanager.cpp \
    highscores.cpp \
    ergebnis.cpp

HEADERS  += mainwindow.h \
    kommunikation.h \
    kommandos.h \
    navdata.h \
    gamepad.h \
    spielerdialog.h \
    datenbankmanager.h \
    highscores.h \
    ergebnis.h

FORMS    += mainwindow.ui \
    spielerdialog.ui \
    ergebnis.ui


win32: LIBS += -lXinput

OTHER_FILES += \
    Anleitung.png \
    Banner.jpg \
    C:/Windows/System32/xinput1_3.dll \
    xinput1_3.dll

RESOURCES += \
    Resourcen.qrc
