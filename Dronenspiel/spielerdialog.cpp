#include "spielerdialog.h"
#include "ui_spielerdialog.h"

SpielerDialog::SpielerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SpielerDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(nameSetzen()));
}

SpielerDialog::~SpielerDialog()
{
    delete ui;
}

QString SpielerDialog::getName() const
{
    return name;
}

void SpielerDialog::setName(const QString &value)
{
    name = value;
}
void SpielerDialog::nameSetzen()
{
    setName(ui->lineEdit->text());
}


