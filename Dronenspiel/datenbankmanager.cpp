#include "datenbankmanager.h"
#include <QDir>
#include <QTime>
#include <QVariant>

/**
 * Konstruktor
 *
 * @brief DatenbankManager::DatenbankManager
 * @param parent
 */

DatenbankManager::DatenbankManager(QObject *parent) :
    QObject(parent)
{
}

DatenbankManager::~DatenbankManager()
{
    closeDB();
}


/**
 * Öffnet Datenbankverbindung
 *
 * @brief DatenbankManager::openDB
 * @return
 */


bool DatenbankManager::openDB()
{
    db = QSqlDatabase::addDatabase("QSQLITE");

#ifdef Q_OS_LINUX
    QString path(QDir::home().path());
    path.append(QDir::separator()).append("my.db.sqlite");
    path = QDir::toNativeSeparators(path);
    db.setDatabaseName(path);
#else
    db.setDatabaseName("my.db.sqlite");

#endif

    return db.open();
}

/**
 * Schließt Datenbankverbindung
 *
 * @brief DatenbankManager::closeDB
 * @return
 */

bool DatenbankManager::closeDB()
{
    db.close();
    if (!db.isOpen()) {
        return true;
    }
    return false;

}

/**
 * Erstellt Highscoretabelle
 *
 * @brief DatenbankManager::HighscoreListeErstellen
 * @return
 */

bool DatenbankManager::HighscoreListeErstellen()
{

    if (db.isOpen()) {
        QSqlQuery qu;
        return qu.exec("create table highscores"
                       "(name varchar(30),"
                       "zeit varchar(30))");
    }
    return false;

}

/**
 *  Liefert die letzte Fehlermeldung der Datenbank zurück
 *
 * @brief DatenbankManager::lastError
 * @return
 */

QSqlError DatenbankManager::lastError()
{

    return db.lastError();
}

/**
 * Schreibt Satz in Highscoretabelle
 *
 * @brief DatenbankManager::satzEinfuegen
 * @param name
 * @param zeit
 * @return
 */


bool DatenbankManager::satzEinfuegen(QString name, QString zeit)
{
    if (db.isOpen()) {
        QSqlQuery qu;
        qu.prepare("insert into highscores values(?,?)");
        qu.addBindValue(name);
        qu.addBindValue(zeit);
        return qu.exec();

    }
    return false;
}

/**
 * Liest die Sätze der Highscore Tabelle aus und gibt diese an das Programm zourück
 *
 * @brief DatenbankManager::TabelleLesen
 * @return
 */

QSqlQuery DatenbankManager::TabelleLesen()
{
    if (db.isOpen()) {
        QSqlQuery qu;
        if(qu.exec("select * from highscores order by zeit asc")){
            return qu;
        }

    }
    return QSqlQuery();
}

/**
 * Prüft ob Tabelle existiert
 *
 * @brief DatenbankManager::tableExists
 * @return
 */

bool DatenbankManager::tableExists()
{
    if(db.isOpen()){
        QSqlQuery qu("SELECT name FROM sqlite_master WHERE type='table' AND name='highscores'");
        qu.exec();

        if ( qu.first()) {
            return true;
        }


    }
    return false;
}
