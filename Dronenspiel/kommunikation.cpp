#include "kommunikation.h"
#include <QDebug>
//#include <QTextStream.h>
#include <QThread>
#include "navdata.h"

#define NAVDATA_HEADER            0x55667788

using namespace std;
//Strukturen auslagern sobald funktionsfähig

Kommunikation::Kommunikation()
{
    sendingSocket = 0;
    receivingSocket = 0;
    cnt = 0;

}

Kommunikation::~Kommunikation()
{
    if (sendingSocket!=0) {
        if (sendingSocket->isOpen()) {
            sendingSocket->close();
        }
        delete sendingSocket;

    }


    if (receivingSocket!=0) {
        if (receivingSocket->isOpen()) {
            receivingSocket->close();
        }
        delete receivingSocket;

    }


    //delete videoSocket;
}

int Kommunikation::VerbindenSender()
{
    //Socket zum Senden der Befehle erstellen
    sendingSocket = new QUdpSocket();
    sendingSocket->connectToHost("192.168.1.1",sendingPort);
    if(sendingSocket->state()==QAbstractSocket::ConnectedState)
    {
        qDebug() << "Sender da" << endl;

    }
    return 0;
}

int Kommunikation::VerbindenEmpfaenger()
{
    receivingSocket = new QUdpSocket(this);
    connect(receivingSocket,SIGNAL(readyRead()),this,SLOT(DatenErhalten()));
    bool res =receivingSocket->bind(/*QHostAddress("192.168.1.1"),*/5554,QUdpSocket::ShareAddress);
    //  receivingSocket->connectToHost("192.168.1.1",receivingPort);

    if(!res){
        qDebug() << "pNdSocket->bind fehlgeschlagen: " << res;
        return -1;
    }else{
        qDebug() << "Empfänger da";
    }

    return 0;
}


//Daten Lesen
void Kommunikation::DatenErhalten()
{

    qDebug() << "readNavdata";

    while (receivingSocket->hasPendingDatagrams()) {

        cnt++;
        int size = receivingSocket->pendingDatagramSize();
        QByteArray datagram;
        datagram.resize(size); //Größe des Dagramms mit Größe des Streams festlegen
        receivingSocket->readDatagram(datagram.data(),size);//In Datagramm lesen
        qDebug() << "Datagram empfangen" << size << "   " << cnt;

        navdata_t* pnavdata = (navdata_t*) datagram.data();
        if(pnavdata->header == NAVDATA_HEADER){
            navdata_option_t* pnavdata_option;
            flugstatus = pnavdata->ardrone_state;
            emit valueChanged(flugstatus);
            pnavdata_option = NULL;
            pnavdata_option = &pnavdata->options[0];
            while(pnavdata_option != NULL){ // Ausführen solange optionen vorhanden sind
                qDebug() << "Optionsize:" <<pnavdata_option->size;

                if (pnavdata_option->size==0) {
                    qDebug() << "Keine Optionen";
                    pnavdata_option = NULL; //Wenn keine Option vorhanden ist Abbruchbedingung setzen

                }
                else{
                    qDebug() << "Options-Tag:"<<pnavdata_option->tag;

                    switch (pnavdata_option->tag) {
                    case 0:{
                        navdata_demo_t nddemo;
                        memcpy(&nddemo,pnavdata_option,sizeof(nddemo));
                        qDebug() << "Batteriestand: " << nddemo.vbat_flying_percentage;     //ungültiger Wert wird ausgelesen                              //^Später mit der Größe des Pakets (navdata_demo_t zu klein)
                        flying_percentage = nddemo.vbat_flying_percentage;
                        emit valueChanged(flying_percentage);
                        break;}

                    case 65535:
                        qDebug() << "TODO CKS";
                        pnavdata_option = NULL;
                        continue;
                        break;



                    default:

                        break;
                    }
                    pnavdata_option += pnavdata_option->size;
                }

            }

        }
        bool fliegt;
        if(flugstatus%2==0){
            fliegt = false;
        }else{
            fliegt = true;
        }



        emit DatenUebertragen(fliegt);

    }


}


void Kommunikation::DatenSenden(QString befehl, int sequenzNummer, QString parameter)
{
    QString nachricht;

    nachricht = befehl+QString::number(sequenzNummer);

    if (befehl != "AT*FTRIM=" && befehl != "AT*COMWDG" && befehl != "AT*CTRL=") {
        nachricht +=','+parameter;
    }

    if(sendingSocket->state()==QAbstractSocket::ConnectedState)
    {
        QTextStream stream(sendingSocket);
        qDebug() << nachricht << endl;
        stream << nachricht+"\r" << endl;
    }



}

void Kommunikation::DatenSenden(QString befehl)
{
    QTextStream stream(sendingSocket);
    if (sendingSocket->state()==QAbstractSocket::ConnectedState) {
        qDebug() << befehl << endl;
        stream << befehl+"\r" <<endl;
    }
}
/**
 * @brief Kommunikation::DatenAnfordern
 *
 * Fordert die NavData der Drohne an
 */

void Kommunikation::DatenAnfordern()
{
    //QTextStream stream(receivingSocket);
    qDebug() << "Datenanforderung\r";
    const char data[] = "\1\0\0\0";   //Datenpaket zum anfordern der NavData
    QHostAddress drohne("192.168.1.1");
    int res =receivingSocket->writeDatagram(data,drohne,5554);
    //QThread::msleep(50);
    if (res==-1) {
        qDebug() << "pNdSocket->writeDatagram fehlgeschlagen";
    }else{
        qDebug() << "Anforderung abgeschickt";
    }
}




