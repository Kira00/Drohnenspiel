#ifndef GAMEPAD_H
#define GAMEPAD_H

#include <windows.h>
#include <QString>
#ifdef Q_OS_WIN
# include <xinput.h>
#endif

typedef struct Flugparameter {
    float vorwaerts;
    float seitwaerts;
    float steigen;
    float rotieren;
}Flugparameter;

typedef struct Kommandoparameter{
    int vorwaerts;
    int seitwaerts;
    int steigen;
    int rotieren;
}Kommandoparameter;


class GamePad
{



public:
    GamePad(int playernr=1);
    bool isConnected();
    void getState();
    int getBefehl(int flugstatus);
    Flugparameter getParameter();


private:
#ifdef Q_OS_WIN

    XINPUT_STATE controllerstate;

#endif
    int controllernr;

};

#endif // GAMEPAD_H
