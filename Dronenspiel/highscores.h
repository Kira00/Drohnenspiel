#ifndef HIGHSCORES_H
#define HIGHSCORES_H

#include <QWidget>
#include <QDialog>
#include <QSqlRecord>
#include <QSqlQuery>

class QGridLayout;
class QScrollArea;
class QScrollBar;
class QLabel;
class QLCDNumber;


class Highscores : public QDialog
{
    Q_OBJECT
public:
    explicit Highscores(QWidget *parent = 0);
    ~Highscores();
    QSqlQuery record;
signals:
    
public slots:
    void paintEvent(QPaintEvent *);
    
private:
    QGridLayout *grid;
    QScrollArea *area;
    QScrollBar *bar;


};

#endif // HIGHSCORES_H
