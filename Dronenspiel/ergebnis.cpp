#include "ergebnis.h"
#include "ui_ergebnis.h"

Ergebnis::Ergebnis(QWidget *parent) :
    QDialog(parent), ui(new Ui::Ergebnis)
{
    setNeu(false);
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(Speichern()));
    connect(ui->checkBox,SIGNAL(stateChanged(int)),this,SLOT(neuesSpiel(int)));
    setSpeichern(false);
}

Ergebnis::~Ergebnis()
{
    delete ui;
}

QString Ergebnis::getZeit() const
{
    return zeit;
}

void Ergebnis::setZeit(const QString &value)
{
    zeit = value;
    ui->lcdNumber->display(zeit);
    update();
}

QString Ergebnis::getName() const
{
    return name;
}

void Ergebnis::setName(const QString &value)
{
    name = value;
    ui->label_2->setText(name);

}

bool Ergebnis::getSpeichern() const
{
    return speichern;
}

void Ergebnis::setSpeichern(bool value)
{
    speichern = value;
}

void Ergebnis::Speichern()
{
    setSpeichern(true);
    close();
}

void Ergebnis::neuesSpiel(int checked)
{
    if (checked > 0) {
        setNeu(true);
    }
    else{
        setNeu(false);
    }
}

bool Ergebnis::getNeu() const
{
    return neu;
}

void Ergebnis::setNeu(bool value)
{
    neu = value;
}
