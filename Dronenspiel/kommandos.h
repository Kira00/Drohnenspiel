#ifndef KOMMANDOS_H
#define KOMMANDOS_H

#include "kommunikation.h"
#include "gamepad.h"
class Kommandos : public QObject
{
    Q_OBJECT
public:
    Kommandos();
    ~Kommandos();

    int getSequenzNumber() const;
    void setSequenzNumber(int value);
    void Starten();
    void Landen();
    void Emergency();
    void Kalibrierung();
    void NavDataConf();
    void Fliegen();
    void Wathchdog();
    void FliegenTastatur(int key=10);
    void FliegenControler(Flugparameter parameter);


    GamePad *getPad() const;
    void setPad(GamePad *value);

public slots:
    void KontrollerAbfragen(bool fliegt);

private:
    void SequenzNumberErhoehen();
    int sequenzNumber;
    Kommunikation *komm;
    unsigned int isl;
    GamePad *pad;
    Flugparameter parameter;

};

#endif // KOMMANDOS_H
