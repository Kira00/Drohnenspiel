#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include "kommandos.h"
#include <QThread>
#include "spielerdialog.h"
#include <QTime>

class QTime;
class DatenbankManager;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int flug;
    void Vorbereitung();
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void keyPressEvent(QKeyEvent *keyevent);
    void HighscoresAusgeben();

    void on_pushButton_3_clicked();
    void AnzeigeAendern();

    void on_neuesSpiel();

    void ergebnisAnzeigen(QString z);

signals:
    void landung(QString z);


private:
    Ui::MainWindow *ui;
    Kommandos *kommand;
    Kommunikation * kom;
    //ListenerThread *listener;
    int z;
    QTime *zeit;
    int f_alt;
    DatenbankManager *dbman;
    bool batteriestop;
    QString zt;




};

#endif // MAINWINDOW_H
