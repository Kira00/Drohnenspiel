#include "highscores.h"
#include <QGridLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QLabel>
#include <QLCDNumber>
#include <QVariant>
#include <QDialogButtonBox>
#include <QList>
#include <QTime>




Highscores::Highscores(QWidget *parent) :
    QDialog(parent)
{

    resize(550,250);
    QFont font = this->font();
    font.setPointSize(20);
    setFont(font);
    grid = new QGridLayout(this);
    area = new QScrollArea(this);
    grid->addWidget(area,0,0,1,2);
    bar = new QScrollBar(this);
    area->addScrollBarWidget(bar,Qt::AlignRight);
    QDialogButtonBox *qdb = new QDialogButtonBox(QDialogButtonBox::Ok,Qt::Horizontal,this);
    QFont f = this->font();
    f.setPointSize(10);
    qdb->setFont(f);
    grid->addWidget(qdb,1,1,1,1);
    connect(qdb,SIGNAL(accepted()),this,SLOT(close()));
    update();
}

Highscores::~Highscores()
{
    delete bar;
    delete area;
    delete grid;
}

void Highscores::paintEvent(QPaintEvent *)
{

    QWidget *wid = new QWidget();

    QGridLayout *gr = new QGridLayout(wid);
    gr->addWidget(new QLabel("Nr.",this),0,0,1,1);
    gr->addWidget(new QLabel("Name",this),0,1,1,1);
    gr->addWidget(new QLabel("Zeit",this),0,2,1,1);
    int i = 0;


    while (record.next()) {
        i++;
        QLabel *nr = new QLabel(QString::number(i),this);

        QLabel *name = new QLabel(record.value(0).toString(),this);
        QLCDNumber *zeit = new QLCDNumber(this);
        zeit->setDigitCount(12);


        zeit->display(record.value(1).toString());
        gr->addWidget(nr,i,0,1,1);
        gr->addWidget(name,i,1,1,1);
        gr->addWidget(zeit,i,2,1,1);

        area->setWidgetResizable(true);
        area->setWidget(wid);
        area->show();


    }

    //delete gr;
    //delete wid;

}

