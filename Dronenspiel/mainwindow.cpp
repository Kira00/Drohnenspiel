#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ergebnis.h"
#include <QKeyEvent>
#include <QBitArray>
#include "datenbankmanager.h"
#include <QFile>
#include <QPixmap>
#include "highscores.h"
#include <QMessageBox>
//#include <wtypes.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dbman = new DatenbankManager();
    dbman->openDB();
    if (!dbman->tableExists()) {
        if(!dbman->HighscoreListeErstellen()){
            qDebug() << dbman->lastError();
        }
    }
    connect(ui->actionVerbinden,SIGNAL(triggered()),this,SLOT(on_neuesSpiel()));
    kommand=0;
    kom = 0;


}

MainWindow::~MainWindow()
{
    //listener->quit();
    //listener->exit(0);
    //delete listener;

    dbman->closeDB();
    delete dbman;
    delete zeit;
    delete kom;
    delete kommand;
    delete ui;

}

void MainWindow::Vorbereitung()
{

    ui->Anleitung->setPixmap(QPixmap(":/new/prefix1/Anleitung.png"));
    ui->banner->setPixmap(QPixmap(":/new/prefix1/BannerWeiss.jpg"));
    setWindowState(Qt::WindowMaximized);
    zeit = new QTime(0,0,0,0);
    QString zt = zeit->toString("hh:mm:ss:zzz");
    ui->lcdNumber->display(zt);
    if(kommand==0){
        kommand = new Kommandos();
        kommand->Kalibrierung();
    }

    if (kom==0) {
        kom = new Kommunikation();
        kom->VerbindenEmpfaenger();

    }

    on_neuesSpiel();



    //listener = new ListenerThread();
    //listener->start();

    z=0;
    flug = 0;
    f_alt = 0;

    //kom = new Kommunikation();
    connect(kom,SIGNAL(valueChanged(quint32)),this,SLOT(AnzeigeAendern()));
    connect(kom,SIGNAL(DatenUebertragen(bool)),kommand,SLOT(KontrollerAbfragen(bool)));
    connect(ui->actionNeues_Spiel,SIGNAL(triggered()),this,SLOT(on_neuesSpiel()));
    connect(this,SIGNAL(landung(QString)),this,SLOT(ergebnisAnzeigen(QString)));
    connect(ui->actionHighscore_Liste,SIGNAL(triggered()),this,SLOT(HighscoresAusgeben()));

    this->setFocus();
    //öffnen der Datenbank


}
/**
 * Startet die Drohne
 *
 * @brief MainWindow::on_pushButton_clicked
 */
void MainWindow::on_pushButton_clicked()
{


    kommand->Starten();
    this->setFocus();

}

/**
 *
 * Lässt die Drohne landen
 *
 * @brief MainWindow::on_pushButton_2_clicked
 */

void MainWindow::on_pushButton_2_clicked()
{
    do{
        kommand->Landen();
    } while(flug == 1);
    this->setFocus();
}

/**
 * Ermöglicht Tastatursteuerung der Drohne
 *
 * @brief MainWindow::keyPressEvent
 * @param keyevent
 */

void MainWindow::keyPressEvent(QKeyEvent *keyevent)
{

    switch (keyevent->key()) {
    case Qt::Key_Space :
        if(flug == 0){
            kommand->Starten(); break;
        }
    case Qt::Key_Alt:

        kommand->Landen(); break;

    case Qt::Key_Backspace:kommand->Emergency(); kom->DatenAnfordern(); kommand->NavDataConf();break;

    case Qt::Key_Up:
        kommand->FliegenTastatur(0); break;         //vorwärts fliegen
    case Qt::Key_W:
        kommand->FliegenTastatur(0); break;         //vorwärts fliegen
    case Qt::Key_Down:
        kommand->FliegenTastatur(1); break;         //rückwärts fliegen
    case Qt::Key_S:
        kommand->FliegenTastatur(1); break;         //rückwärts fliegen
    case Qt::Key_Left:
        kommand->FliegenTastatur(2); break;         //nach links fliegen
    case Qt::Key_A:
        kommand->FliegenTastatur(2); break;         //nach links fliegen
    case Qt::Key_Right:
        kommand->FliegenTastatur(3); break;         //nach rechts fliegen
    case Qt::Key_D:
        kommand->FliegenTastatur(3); break;         //nach rechts fliegen
    case Qt::Key_Shift:
        kommand->FliegenTastatur(6);break;          //nach links drehen
    case Qt::Key_8:
        kommand->FliegenTastatur(4);break;          //steigen
    case Qt::Key_2:
        kommand->FliegenTastatur(5); break;         //sinken
    case Qt::Key_E:
        kommand->FliegenTastatur(7); break;         //nach rechts drehen

    }
}




void MainWindow::on_pushButton_3_clicked()
{
    kommand->Emergency();
    this->setFocus();
}

void MainWindow::AnzeigeAendern()
{

    qDebug() << "Anzeige aktualisieren";
    if(kom->cnt<=1 || kom->flying_percentage>100){
        kommand->NavDataConf();
    }
    flug = kom->flugstatus%2;           //gerader Wert => Drohne am boden(Bit Nr.0)




    if(flug == 1){
        if (zeit->msec() == 0) {
            zeit = new QTime(QTime::currentTime());
        }

        int msec = QTime::currentTime().msecsTo(*zeit);

        QTime t(0,0,0);
        t = t.addMSecs(-1 * msec);

        ui->label_2->setText("fliegt");
        zt = t.toString("hh:mm:ss:zzz");
        ui->lcdNumber->display(zt);
    }

    else{
        ui->label_2->setText("am Boden");
        if(f_alt == 1){

            emit landung(zt);
        }

    }

    f_alt = flug;

    this->ui->label_4->setText(QString::number(kom->flying_percentage)+"%");

    if (kom->flying_percentage<= 20) {
        if (!batteriestop) {
            kommand->Landen();
            QMessageBox::information(this,"Sicherheitslandung",
                                     "Drohne wird zur Sicherheit gelandet\n Batteriestand zu niedrig",QMessageBox::Ok);
            batteriestop = true;
        }
    }else{
        batteriestop = false;
    }




    if(kommand->getPad()->isConnected()){
        ui->label_9->setText("ja");
    }else{
        ui->label_9->setText("nein");
    }


    z++;

    if(z>=30){
        kommand->Wathchdog();
        z=0;
    }
    update();

}

void MainWindow::on_neuesSpiel()
{
    SpielerDialog sp(this);

    sp.exec();
    if(sp.getName()!=""){
        ui->label_10->setText(sp.getName());
    }
    else{
        ui->label_10->setText("Anonymous");
    }
    kom->cnt = 0;
    kom->DatenAnfordern();
    kommand->setSequenzNumber(0);
    if(zeit != 0){
        delete zeit;
        zeit = new QTime(0,0,0,0);
    }

    //emit landung(zeit->toString("hh:mm:ss:zzz"));
    //ergebnisAnzeigen("00:05:38:612");

}

void MainWindow::ergebnisAnzeigen(QString z)
{
    Ergebnis e(this);
    e.setName(ui->label_10->text());
    e.setZeit(z);
    e.exec();
    if (e.getSpeichern()) {
        if(!dbman->satzEinfuegen(ui->label_10->text(),z)){
            qDebug() << dbman->lastError().text();
        }
    }

    if (e.getNeu()) {
        on_neuesSpiel();
    }
}


void MainWindow::HighscoresAusgeben()
{
    QSqlQuery rec = dbman->TabelleLesen();
    Highscores h(this);
    h.record = rec;
    h.exec();

}
