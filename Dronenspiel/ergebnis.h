#ifndef ERGEBNIS_H
#define ERGEBNIS_H

#include <QDialog>

namespace Ui {
class Ergebnis;
}

class Ergebnis : public QDialog
{
    Q_OBJECT
    
public:
    explicit Ergebnis(QWidget *parent = 0);
    ~Ergebnis();
    
    QString getName() const;
    void setName(const QString &value);

    QString getZeit() const;
    void setZeit(const QString &value);

    bool getSpeichern() const;
    void setSpeichern(bool value);


    bool getNeu() const;
    void setNeu(bool value);

public slots:
    void Speichern();
    void neuesSpiel(int checked);

private:
    Ui::Ergebnis *ui;
    QString name;
    QString zeit;
    bool speichern;
    bool neu;
};

#endif // ERGEBNIS_H
