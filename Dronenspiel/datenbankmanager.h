#ifndef DATENBANKMANAGER_H
#define DATENBANKMANAGER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QFile>
#include <QSqlQuery>
#include <QSqlRecord>

class DatenbankManager : public QObject
{
    Q_OBJECT
public:
    explicit DatenbankManager(QObject *parent = 0);
    ~DatenbankManager();
    bool openDB();
    bool closeDB();
    bool HighscoreListeErstellen();
    QSqlError lastError();
    bool satzEinfuegen(QString name, QString zeit);
    QSqlQuery TabelleLesen();
    bool tableExists();
    
signals:
    
public slots:

private:
    QSqlDatabase db;
    
};

#endif // DATENBANKMANAGER_H
