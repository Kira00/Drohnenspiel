#include "gamepad.h"
#include <QtDebug>
#include <fstream>

/**
 * @brief Gamepad::Gamepad
 * @param playernr
 *
 * Konstruktor mit nummer zur identifizierung des Kontrollers
 */

GamePad::GamePad(int playernr)
{


    controllernr = playernr-1;


}

/**
 * @brief Gamepad::isConnected
 * @return bool
 *
 *Liefert ob Controller verbunden ist
 *
 *True = Controller ist verbunden
 */

bool GamePad::isConnected()
{
#ifdef Q_OS_WIN
    std::ifstream dllfile("xinput1_3.dll");
    if (dllfile) {
        DWORD result = XInputGetState(controllernr,&controllerstate);

        if(result == 0){
            return true;
        }else{
            return false;
        }
    }else
    {
        return false;
    }
#else
    return false;
#endif
}

void GamePad::getState()
{

    //   ZeroMemory(0,sizeof(XINPUT_STATE));
#ifdef Q_OS_WIN
    std::ifstream dllfile("xinput1_3.dll");
    if (dllfile) {
        XInputGetState(controllernr,&controllerstate);
    }
#endif
}
/**
 * @brief Gamepad::getBefehl
 * @param flugstatus
 * @return int
 *
 * Liefert integer wert der einen Tastendruck repräsentiert
 */


int GamePad::getBefehl(int flugstatus)
{
    qDebug() << "getBefehl";
#ifdef Q_OS_WIN
    std::ifstream dllfile("xinput1_3.dll");
    if (dllfile) {
        getState();
        if(isConnected()){
            getState();

            //Wenn kein Button gedrückt ist müssen die werte nicht ausgelesen werden

            if (controllerstate.Gamepad.wButtons & XINPUT_GAMEPAD_A) {
                if (flugstatus == 0) {
                    return 0;
                }else{
                    return 1;
                }
            }

            if(controllerstate.Gamepad.wButtons & XINPUT_GAMEPAD_B){
                return 1;
            }

            if(controllerstate.Gamepad.wButtons & XINPUT_GAMEPAD_X){
                return -2;
            }



            return 2;


        }
    }
    return -1;
#endif


    return -1;
}

/**
 *
 * Liefert die Werte der Analogsticks und der Trigger in einer Struktur
 * @brief Gamepad::getParameter
 * @return Flugparameter
 */

Flugparameter GamePad::getParameter()
{
    double flugw=32768;     //positiver Maximalwert (negativer=-32768) genauigkeit höher als benötigt
    Flugparameter par;

#ifdef Q_OS_WIN

    std::ifstream dllfile("xinput1_3.dll");
    if (dllfile) {
        par.vorwaerts=(controllerstate.Gamepad.sThumbLY)/flugw*(-1.0);
        par.seitwaerts=controllerstate.Gamepad.sThumbLX/flugw*1.0;
        par.steigen = ((float)controllerstate.Gamepad.bRightTrigger - (float)controllerstate.Gamepad.bLeftTrigger); //256=> maxwert der trigger
        par.steigen /= 256;
        par.rotieren=controllerstate.Gamepad.sThumbRX/flugw*1.0;


        //Überprüfung der "Dead Zone" fehlerhafte Werte die auch bei neutraler Position der Sticks geliefert werden
        //Problem:
        //  7000 => Begin der regulären Werte
        //  6000 => vielleicht zu kleiner Bereich

        if(controllerstate.Gamepad.sThumbLY < 6000 && controllerstate.Gamepad.sThumbLY > -6000){
            par.vorwaerts=0;
        }
        if(controllerstate.Gamepad.sThumbLX < 6000 && controllerstate.Gamepad.sThumbLX > -6000){
            par.seitwaerts=0;
        }
        if(controllerstate.Gamepad.sThumbRX < 6000 && controllerstate.Gamepad.sThumbRX > -6000){
            par.rotieren=0;
        }

        //Geschwindigkeitsdrosselung
        if (par.vorwaerts>=0.5) {
            par.vorwaerts=0.5;
        }
        else if(par.vorwaerts<=-0.5){
            par.vorwaerts = -0.5;
        }
        if (par.seitwaerts>=0.5) {
            par.seitwaerts=0.5;
        }
        else if(par.seitwaerts<=-0.5){
            par.seitwaerts= -0.5;
        }
    }
    else{
        par.vorwaerts=-0;
        par.seitwaerts = 0;
        par.steigen=0;
        par.rotieren=0;
    }


#else
    par.vorwaerts=-0.66;
    par.seitwaerts = 1;
    par.steigen=0.5;
    par.rotieren=0;
#endif


    return par;

}
