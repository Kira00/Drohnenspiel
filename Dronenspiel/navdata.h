#ifndef NAVDATA_H
#define NAVDATA_H
#include <QMatrix3x3>
#include <QVector3D>

typedef struct _navdata_option_t { //Bestandteile einer Option
    quint16  tag;
    quint16  size;
    quint8   data[];
private:
    _navdata_option_t(_navdata_option_t const &);
    _navdata_option_t& operator=(_navdata_option_t const &);
} navdata_option_t;

typedef struct _navdata_t { //Bestandteile der Navdata
    quint32    header;
    quint32    ardrone_state;
    quint32    sequence;
    qint32      vision_defined;

    navdata_option_t  *options;

private:
    _navdata_t(_navdata_t const &);
    _navdata_t& operator=(_navdata_t const &);
} __attribute__ ((packed)) navdata_t;

typedef struct _navdata_demo_t {
    quint16    tag;
    quint16    size;
    quint32    ctrl_state;
    quint32    vbat_flying_percentage;

    float      theta;
    float      phi;
    float      psi;

    quint32    altitude;

    float      vx;
    float      vy;
    float      vz;

    quint32    num_frames;

    // Camera parameters compute by detection

    //QMatrix3x3  detection_camera_rot;   /*!<  Deprecated ! Don't use ! */
    //QVector3D   detection_camera_trans; /*!<  Deprecated ! Don't use ! */
    //quint32	  detection_tag_index;    /*!<  Deprecated ! Don't use ! */

    quint32	  detection_camera_type;  /*!<  Type of tag searched in detection */

    // Camera parameters compute by drone
    //QMatrix3x3  drone_camera_rot;		  /*!<  Deprecated ! Don't use ! */
    //QVector3D   drone_camera_trans;	  /*!<  Deprecated ! Don't use ! */

    /* ACHTUNG: hier fehlt noch eine Menge */
} __attribute__ ((packed)) navdata_demo_t;




#endif // NAVDATA_H
