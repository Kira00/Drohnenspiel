#ifndef SPIELERDIALOG_H
#define SPIELERDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class SpielerDialog;
}

class SpielerDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SpielerDialog(QWidget *parent = 0);
    ~SpielerDialog();

    QString getName() const;
    void setName(const QString &value);

public slots:
    void nameSetzen();

private:
    Ui::SpielerDialog *ui;
    QString name;
};

#endif // SPIELERDIALOG_H
