#include "kommandos.h"


Kommandos::Kommandos()
{
    setSequenzNumber(1);
    komm = new Kommunikation();
    isl = 0;
    //Setzen der "AT*REF"-Variable
    isl |= 1 << 18;
    isl |= 1 << 20;
    isl |= 1 << 22;
    isl |= 1 << 24;
    isl |= 1 << 28;

    qDebug() << isl << endl;
    komm->VerbindenSender();
    pad = new GamePad(1);
    if(pad->isConnected()){
        qDebug() << "Kontroller da";
    }


}

int Kommandos::getSequenzNumber() const
{
    return sequenzNumber;
}

void Kommandos::setSequenzNumber(int value)
{
    sequenzNumber = value;
}

void Kommandos::Starten(){

    isl |= 1 << 9;

    //komm->DatenErhalten();
    komm->DatenSenden("AT*REF=",getSequenzNumber(),QString::number(isl)); //Starten
    SequenzNumberErhoehen();
    isl &= ~(1 << 9);
}

void Kommandos::Landen()
{
    komm->DatenSenden("AT*REF=",getSequenzNumber(),QString::number(isl)); //Landen
    SequenzNumberErhoehen();

}

void Kommandos::Emergency()
{

    isl |= 1 << 8;
    komm->DatenSenden("AT*REF=",getSequenzNumber(),QString::number(isl)); //Notfallstop
    SequenzNumberErhoehen();
    QThread::msleep(50);
    isl &= ~(1 << 8);

}

void Kommandos::Kalibrierung()
{
    komm->DatenSenden("AT*FTRIM=",getSequenzNumber(),0); //Sensor kalibrieren(parallel zum boden)
    SequenzNumberErhoehen();

}

void Kommandos::NavDataConf()
{
    komm->DatenSenden("AT*CONFIG=",1,"\"general:navdata_demo\",\"TRUE\""); //Anforderung der umfangreichen Daten
    komm->DatenSenden("AT*CTRL=",0,"");
    SequenzNumberErhoehen();
}

void Kommandos::Fliegen()
{
    komm->DatenSenden("AT*PCMD=",getSequenzNumber(),"");
    SequenzNumberErhoehen();
}

/**
 *@brief Kommandos::Wathchdog()
 *
 *Befehl stellt sicher, dass die Drohne weiterhin auf Befehle hört
 */

void Kommandos::Wathchdog()
{
    komm->DatenSenden("AT*COMWDG");
}
/**
 * @brief Kommandos::FliegenTastatur
 * @param key
 *
 * Senden der Drohnenbefehle, über Tastenbefehl
 */
void Kommandos::FliegenTastatur(int key)
{
    QString hexdez;
    int wert=0;

    if(key%2==0){
        float f= -0.5;
        QByteArray array(reinterpret_cast<const char*>(&f),sizeof(f));
        wert = *reinterpret_cast<int*>(array.data()); //Geschwindigkeit der einzelnen Aktionen(vorwärts,runter,links)
    }else{
        float f= 0.5;
        QByteArray array(reinterpret_cast<const char*>(&f),sizeof(f));
        wert = *reinterpret_cast<int*>(array.data());//Geschwindigkeit der einzelnen Aktionen(rückwärts,hoch,rechts)
    }

    QString kopf = "AT*PCMD=";

    switch(key){
    case 0 :
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,"+QString::number(wert)+",0,0"); //vorwärts fliegen
        break;
    case 1:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,"+QString::number(wert)+",0,0"); // rückwärts fliegen
        break;
    case 2:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,"+QString::number(wert)+",0,0,0"); // links fliegen
        break;
    case 3:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,"+QString::number(wert)+",0,0,0"); //rechts fliegen

    case 4:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,0,"+QString::number(wert)+",0"); //sinken;
        break;
    case 5:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,0,"+QString::number(wert)+",0"); //steigen
        break;
    case 6:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,0,0,"+QString::number(wert));//links drehen
        break;
    case 7:
        komm->DatenSenden(kopf,getSequenzNumber(),"1,0,0,0,"+QString::number(wert));//rechts drehen
        break;
    default:
        komm->DatenSenden(kopf,getSequenzNumber(),"0,0,0,0,0"); // schweben
        break;

    }
    SequenzNumberErhoehen();
}

GamePad *Kommandos::getPad() const
{
    return pad;
}

void Kommandos::setPad(GamePad *value)
{
    pad = value;
}

/**
 * @brief Kommandos::FliegenControler
 * @param parameter
 *
 * Casted Werte der Struktur parameter von float => hexdez => int
 * und schickt diese an die Drohne
 */
void Kommandos::FliegenControler(Flugparameter parameter)
{

    Kommandoparameter par;


    QByteArray vorwaerts(reinterpret_cast<const char*>(&parameter.vorwaerts),sizeof(parameter.vorwaerts));
    par.vorwaerts = *reinterpret_cast<int*>(vorwaerts.data());

    QByteArray seitwaerts(reinterpret_cast<const char*>(&parameter.seitwaerts),sizeof(parameter.seitwaerts));
    par.seitwaerts = *reinterpret_cast<int*>(seitwaerts.data());

    QByteArray steigen(reinterpret_cast<const char*>(&parameter.steigen),sizeof(parameter.steigen));
    par.steigen = *reinterpret_cast<int*>(steigen.data());

    QByteArray rotieren(reinterpret_cast<const char*>(&parameter.rotieren),sizeof(parameter.rotieren));
    par.rotieren = *reinterpret_cast<int*>(rotieren.data());

    //bool ok;
    //qDebug() << QString::number(hexdez.toFloat());
    if(par.vorwaerts>0 || par.seitwaerts > 0 || par.steigen>0 || par.rotieren>0)
    {
        komm->DatenSenden("AT*PCMD=",getSequenzNumber(),"1,"+QString::number(par.seitwaerts)+","
                          +QString::number(par.vorwaerts)+","+QString::number(par.steigen)+","
                          +QString::number(par.rotieren));

    }
    else{
        komm->DatenSenden("AT*PCMD=",getSequenzNumber(),"0,0,0,0,0");

    }
    /*
    qDebug() << "AT*PCMD=" << QString::number(getSequenzNumber()) << "1,1,"<< QString::number(par.seitwaerts)<<","
            << QString::number(par.vorwaerts)<<","<<QString::number(par.steigen)<<","
            << QString::number(par.rotieren);
*/
    SequenzNumberErhoehen();
}
/**
 * @brief Kommandos::KontrollerAbfragen
 * @param fliegt
 *
 * Ruft Kontroller ab.
 * fliegt: TRUE = Drohne befindet sich in der Luft
 */


void Kommandos::KontrollerAbfragen(bool fliegt)
{

    if(pad->isConnected()){
        qDebug() << "Kontroller abfragen";
        int command = pad->getBefehl(fliegt);

        switch (command) {
        case 0: Starten();
            qDebug() << "Starten"; break;
        case 1: Landen();
            qDebug() << "Landen";break;
        case 2:
            parameter = pad->getParameter();
            //if(fliegt){
            FliegenControler(parameter);
            //}
            break;
        case -2:
            Emergency();
        default: Wathchdog();break;
        }


    }
    /*
    parameter = pad->getParameter();
    FliegenControler(parameter);
*/
}


Kommandos::~Kommandos()
{
    delete komm;
    delete pad;
}

void Kommandos::SequenzNumberErhoehen(){
    setSequenzNumber(getSequenzNumber()+1);
}


