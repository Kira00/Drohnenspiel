#ifndef KOMMUNIKATION_H
#define KOMMUNIKATION_H
#include <sys/types.h>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QThread>
#include <iostream>

using namespace  std;


class Kommunikation : public QObject
{
    Q_OBJECT
public:
    Kommunikation();
    int VerbindenSender();
    int VerbindenEmpfaenger();
    void DatenAnfordern();
    void DatenSenden(QString befehl, int sequenzNummer, QString parameter);
    void DatenSenden(QString befehl);
    ~Kommunikation();
    int cnt;

    quint32 flugstatus;
    quint32 flying_percentage;
public slots:

    void DatenErhalten();
signals:

    void valueChanged(quint32 value);
    void DatenUebertragen(bool);
private:
    const static int sendingPort=5556;
    const static int receivingPort=5554;
    const static int videoPort=5555;
    QUdpSocket* sendingSocket;
    QUdpSocket* receivingSocket;
    QTcpSocket* videoSocket;


};

#endif // KOMMUNIKATION_H
